### slicing

    #include <iostream>
    
    struct base {
        base() = default;
        base(base const &) {}
        virtual void print() {
            std::cout << "base\n";
        }
    };
    
    struct derived : base {
        void print() {
            std::cout << "derived\n";
        }
    };
    
    int main() {
        derived d;
        base &b = d;
        b.print(); // "derived"
        
        base b1 = d; // slicing
        b1.print(); // "base"
    }
    
### how to fix?

```
struct base {
    base(base const &) = delete;
    virtual void print() {
        std::cout << "base\n";
    }
};
``` 

### cast

```
(D) A; // new object of D;
(D&) A; // no new object of D
```

# Exceptions
### return codes

```
int do_something() {                // return codes
    FILE * file = fopen("1.txt");
    if (not file) {
        return false;
    }

    ssize_t bytes_read = fread(..., file);
    if (bytes_read < 0) {
        return false;
    }

    ssize_t bytes_read = fread(..., file);
    if (bytes_read < 0) {
        return false;
    }

    ssize_t bytes_read = fread(..., file);
    if (bytes_read < 0) {
        return false;
    }

    ssize_t bytes_read = fread(..., file);
    if (bytes_read < 0) {
        return false;
    }

    fclose(file);
    return true;
}

```

leads to longer and more error-prone code

To express the concept exceptions were developed:

```$xslt
void f() {
    if (...)
        throw runtime_error("f() failed")
}
```

exception message will be thrown through many layers until try-catch blocks:

```
int main() {
    try {
        f();
    } catch (runtime_error const & e) {
        ...
    }
}
```

* Derived errors may be caught as base ones;
* Any type except primitives may be thrown
* There is a universal catch clause that catches everything: `catch (...)`

```
...
catch (... & e) {
    throw; // throws current error -- e
}
```

### slicing in throw clause

    #include <string>
    #include <iostream>

    struct base {
        virtual std::string msg() const {
            return "base";
        }
    };

    struct derived : base {
        std::string msg() const {
            return "derived";
        }
    };

    int main() {
        try {
            throw derived();
        } catch (const base &e) {
            std::cout << e.msg() << std::endl; // "derived"
        }

        try {
            throw derived();
        } catch (base e) {
            std::cout << e.msg() << std::endl; // "base" -- slicing
        }
    }

### dynamic/static type throwing

    try {
        try {
            throw derived();
        } catch (base const &e1) {
            std::cout << e1.msg(); // derived
            throw e1;               // throws static type
        }
    } catch (base const &e2) {
        std::cout << e2.msg();      // base
    }
but:

    try {
        try {
            throw derived();
        } catch (base const &e1) {
            std::cout << e1.msg(); // derived
            throw;                 // throws dynamic type
        }
    } catch (base const &e2) {
        std::cout << e2.msg();     // derived
    }

throw copies on-stack exception to some place in memory

## exceptions use-cases

* convey additional information in case of error
* things that are not errors, but exceptions:
    * there is a need to be able to interrupt some computation: it is done via exceptions
* if situation calls for pushing info back up stack through many calls
* exceptions are zero cost when no exceptions occur and cost around 10 times more than return codes otherwise
* (if call chain consists of one link -- exceptions are probably not the best tool)
* (debugger stops on exceptions, which might be mildly infuriating)

## about RAII

* problem with return codes is more boilerplate
* no finally clauses, RAII (destructors) instead -- resource allocation is initialization:

    ```
    struct file_descriptor {
        FILE *file;

        file_descriptor() = delete;

        file_descriptor(std::string filename) {
            file = fopen(filename.c_str(), "r");
        }

        ~file_descriptor() {
            fclose(file);
        }
    };
    ```

* initialization lists

    ```
    struct config_file {
        config_file() : f1("1.txt"),    // if throws -- constructor throws
                        f2("2.txt"),    // if throws -- f1 destructor is called and then constructor throws
                        f3("3.txt") {
            ...                         // if body throws all fields destructors are called and then constructor throws
        }

        file_descriptor f1;
        file_descriptor f2;
        file_descriptor f3;
    };
    ```

* destructors of fields are called in the order of their declaration, thus it's better to call constructors of fields in initializer list in the same order ([actual reason](https://stackoverflow.com/a/12222454/7190191))

### smart pointers

```
int main() {
    int *a = new int(5);
    int *b = new int(6);    // if new throws -- a is not freed

    delete b;
    delete a;
}
```
There is a special container to deal with the problem -- `std::unique_ptr`.
Possible implementation
```
template<typename T>
struct unique_ptr {
    unique_ptr() : p(nullptr) {}
    unique_ptr(T *p) : p(p) {}

    unique_ptr(unique_ptr const &) = delete;
    unique_ptr &operator=(unique_ptr const &) = delete;

    ~unique_ptr() { delete p; }

    T &operator*() const { return *p; }
    T *operator->() const { return p; }

private:
    int *p;
};

int main() {
    unique_ptr<int> a(new int(5));
    unique_ptr<int> b(new int(6));
    std::cout << *a << " " << *b << std::endl;
}
```
Suppose the following situation:
```
void f(unique_ptr<int> p);

int main() {
    int *q = new int(5);
    f(q);
    f(q);
    delete q;
}
```
In this case `delete q` will be called three times.
`explicit` keyword prohibits implicit construction of `unique_ptr` out of `*T`;

It is important to always think if constructor should not be explicit

complete unique_ptr:
```
template<typename T>
struct unique_ptr {
    unique_ptr() : p(nullptr) {}
    unique_ptr(T *p) : p(p) {}

    unique_ptr(unique_ptr const &) = delete;
    unique_ptr &operator=(unique_ptr const &) = delete;

    ~unique_ptr() { delete p; }

    T &operator*() const { return *p; }
    T *operator->() const { return p; }

    T *get() const { return p; }
    void reset(T *q) {
        delete p;
        p = q;
    }
    T* release() {
        T* tmp = p;
        p = nullptr;
        return tmp;
    }

private:
    int *p;
};
```

### more examples

```
struct string {
    string() : data(strdup("")),
               size(0),
               capacity(0) {}

    string(char const *s) : data(strdup(s)),
                            size(strlen(s)),
                            capacity(size) {}

    string(string const &other) {
        //...
    }

    ~string() {
        free(data);
    }

    string &operator=(string const &rhs) {
        if (&rhs == this) {
            return *this;
        }

        char *newdata = (char *) malloc(rhs.size);
        free(data);

        size = rhs.size;
        capacity = rhs.size;

        data = newdata;
        memcpy(data, rhs.data, size);

        return *this;
    }

    char *data;
    size_t size;
    size_t capacity;
};
```
1. nothrow guarantee -- doesn't throw
1. strong guarantee -- if throws, state is unchanged
1. weak guarantee -- if throws, state is unspecified but valid
### safe `operator=` with swap idiom:

```
    string &operator=(string rhs) {
        swap(rhs);
        return *this;
    }

    void swap(string &other) {
        std::swap(data, other.data);
        std::swap(size, other.size);
        std::swap(capacity, other.capacity);
    }
```
### making a function strong guaranteed

```
void f_basic(string &);

void f_string(string &s) {
    string copy = s;
    f_basic(copy);
    s.swap(copy);
}
```
