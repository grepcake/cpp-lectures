# auto

    std::vector<int> v;
    auto it = v.begin();
    
`auto` type is deducted the same way as in templates:
    
    template <typename T>
    void b(T);
    
# anonymous functions / lambdas

    template <typename It, typename Comp>
    void sort(It first, It last, Comp comp);
    
What is `Comp`?

* it might be a function pointer:

        bool abs_less(int a, int b) {return abs(a) < abs(b);}
        …
        sort(begin, end, abs_less);
        

* it might be a named functor:

        struct abs_less {
            bool operator()(int a, int b) const {…}
        }
        …
        sort(begin, end, abs_less);
        
    
 * it might be an anonymous functor – lambda:
 
        sort(begin, end, [](int a, int b) {return abs(a) < abs(b);});
        
    It is basically the same thing as a named functor but has no name
    
### lambda syntax

    [&a, b] (int g) mutable -> bool { … };
     ^(1)     ^(2)    ^(3)    ^(4)   ^(5)
     
`(1)` is a capture list. If a name is preceded with `&` (`&a`), it is captured by reference, else – by value. Only local variables might be captured. To capture every local variable by reference use `[&]`; to capture every local variable by value use `[=]`.

`(2)` is a parameter list.

`(3)` optional `mutable` modificator. lambda is `const` by default.

`(4)` is an optional return type. If it might be deduced from the body, it might be omitted.

`(5)` is a function body.

### nitpicks

The following doesn't compile:

    auto cmp;
    if (…)
        cmp = [](int a, int b) {return a < b;}
    else
        cmp = [](int a, int b) {return a > b;}

`std::function` might help here.

    std::function<bool(int, int)> cmp;
    if (…)
        cmp = [](int a, int b) {return a < b;}
    else 
        cmp = [](int a, int b) {return a > b;}
        
However, the problem with `std::function` is that it doesn't allow to see what a function actually is at compile-time. Thus, unlike lambdas, `std::function` doesn't allow for compile-time optimizations.

### possible implemention of std::function

This implementation allows for small-object optimization:

    template <typename F>
    void destroy_impl(void *p) {delete (F*) p;}

    struct function {
        using destroy_t = void(*)(void*);
        
        template <typename F>
        function(F f)
             : ptr(new F(f))
             , destroy(destroy_impl<F>){}
             
        ~function() {
            destroy(ptr);
        }

    private:
         void *ptr;
         destroy_t destroy;
    }


This one uses type erasure:

    struct function::concept {
        virtual ~concept() {}
        
        virtual std::unique_ptr<concept> copy() const {}
        
        virtual bool call(int a, int b) const;
    }
    
    template<typename F>
    struct function::model : concept {
        model(F f) : f(std::move(f)) {}

        std::unique_ptr<concept> copy() const override {
            return std::make_unique<model<F>>(f);
        }
        
        bool call(int a, int b) const override { return f(a, b); }
        
        F f;
    }

    struct function {
        using destroy_t = void(*)(void*);
        
        template <typename F>
        function(F f)
             : ptr(std::make_unique<model<F>>(std::move(f))) {}
             
        function(function const& other)
             : ptr(other.ptr->copy()) {}
             
        bool operator()(int a, int b) const {
            return ptr->call(a, b);
        }
             
        ~function() {
            destroy(ptr);
        }

    private:
         std::unique_ptr<concept> ptr;
    }

# variadic templates

    template <typename T, typename ...Args>
    unique_ptr<T> make_unique(A &&...a) {
        return std::unique_ptr<T>(new T(std::forward<A>...));
    }

If multiple variadic templates are present, they are expanded in packs:

`f(a, b)...` -> `f(a₀, b₀), f(a₁, b₁),..`

It is possible to do recursive compile-time calculations with variadic templates:

    template <size_t N, typename T0, typename ...Ts>
    struct at {
        using type = typename at<N-1, Ts...>::type;
    };

    template <typename T0, typename ...Ts>
    struct at<0, T0, Ts...> {
        using type = T0;
    };

----

#### possible std::bind implementation

    template <size_t ...Indices>
    struct index_tuple
    {};

    template <size_t N, size_t ...Indices>
    struct make_index_tuple {
        using type = make_index_tuple<N-1, N-1, Indices>::type;
    };

    template <size_t ...Indices>
    struct make_index_tuple<0> {
        using type = index_tuple<Indices...>;
    };

    template <typename F, typename ...Bs>
    struct bind_t<F, Bs...> bind(F f, Bs ...bs) {
        return bind_t<F, Bs...>(std::move(f), std::move(bs)...);
    };

    template <typename F, typename ...Bs>
    struct bind_t {
        bind_t(F f, Bs ...bs)
            : f(std::move(f)),  gs(std::move(bs)...) {}

        F f;
        std::tuple<G<Bs>...> gs;

        template <typename ...As>
        auto operator(As const &...as)() const {
            return call(make_index_tuple<sizeof...(Bs)>(), as...);
        }

        template <typename ...As, size_t ...Indices>
        auto call(index_tuple<Indices...>, As const &...as) {
            return f(get<Indices>(gs)(as...)...);
        }
    };

    template <typename T>
    struct G {
        G(T value) : value(std::move(value)) {}

        template <typename ...Args>
        T operator()(As const &...as) {
            return value;
        }

        T value;
    };

    template <size_t N>
    struct G<std::placeholder<N>> {
        template <typename A, typename ...As>
        auto operator()(A const &a1, As const &...as) {
            return G<N-1>()(as);
        }
    };

    template <>
    struct G<std::placeholder<0>> {
        template <typename A, typename ...As>
        auto operator(A const &a1, As const &...as) {
            return a1;
        }
    };
