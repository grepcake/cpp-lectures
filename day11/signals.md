## Signals

> Also known as listeners and observers.
> A slot is a functional object to react on a signal.

Possible implementation:

    struct signal {
        using slot = std::function <void(…)>;

        void connect(slot s) {
            slots.push_back(std::move(s));
        }

        void operator()() const {
            for (auto &&s: slots) {
                try { s(); }
                catch (...) { log("…"); }
            }
        }

        std::vector<slot> slots;
    };

This approach makes it impossible to disconnect a slot from a signal.

Another way would be to use some sort of id key:

    struct signal {
        struct connection;
        using slot = std::function <void(…)>;

        connection connect(slot s) {
            slots.push_back(std::move(s));
            return {std::prev(slots.end())};
        }

        void disconnect(connection c) {
            slots.erase(c.it);
        }

        void operator()() const {
            for (auto &&s: slots) {
                try { s(); }
                catch (...) { log("…"); }
            }
        }

        std::list<slot> slots;
    };

    struct signal::connection {
        connection(std::list<slot>::iterator it) : it(it) {}

      private:
        std::list<slot>::iterator it;
    }


The problem arising here is disconnection from a deleted signal.
Another one is if there is disconnection in the slot body.
The solution might be to make a copy before iterating.

        void operator()() const {
            std::vector<slot> copy(slots.begin(), slots.end());
            for (auto &&s: copy) {
                try { s(); }
                catch (...) { log("…"); }
            }
        }

However, in this case some slots will be called even if they are disconnected.
Thus, let's store a bool indicating if a slot has been deleted:

    struct signal {
        struct connection;
        using slot = std::function <void(…)>;

        connection connect(slot s) {
            slots.emplace_back(true, std::move(s));
            return {std::prev(slots.end())};
        }

        void disconnect(connection c) {
            it->first = false;
        }

        void operator()() const {
            for (auto &&s: slots) {
                if (s.first) { s.second(); }
            }
            // erase here all s that have s.first == false
        }

        std::list<std::pair<bool, slot>> slots;
    };

A problem with disconnection in the recursion tree.
a solution:

    struct signal {
        struct connection;
        using slot = std::function <void(…)>;

        connection connect(slot s) {
            slots.emplace_back(true, std::move(s));
            return {std::prev(slots.end())};
        }

        void disconnect(connection c) {
            c.it->first = false;
            if (rec_counter == 0) {
                slots.erase(c.it);
            }
        }

        void operator()() const {
            ++rec_counter;
            for (auto &&s: slots) {
                if (s.first) { s.second(); }
            }
            --rec_counter;
            if (rec_counter == 0) {
                // erase here all s, such that have s.first == false
            }
        }

        size_t rec_counter = 0;
        std::list<std::pair<bool, slot>> slots;
    };


##### Reentrancy

> The quality of code of being executed multiple times.
