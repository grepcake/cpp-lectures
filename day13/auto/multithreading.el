(TeX-add-style-hook
 "multithreading"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:orgbc19c30"
    "sec:orgca0c0b0"
    "sec:org032c20b"
    "sec:orgee653c1"
    "sec:org635c323"
    "sec:orga047c56"
    "sec:orge72ba8b"
    "sec:orga3c4b5b"
    "sec:org5c775ea"
    "sec:orga056dc9"
    "sec:org8d47bc6"
    "sec:org881ddc8"
    "sec:org6e020a1"
    "sec:orgc183852"
    "sec:org64ac61a"
    "sec:org5cc7e48"
    "sec:org46ed657"
    "sec:orga2e8595"
    "sec:org8b00831"
    "sec:org0f819a6"))
 :latex)

