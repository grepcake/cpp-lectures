## side note

### memory allocation

mmap -- map pages of memory
`pa = mmap(addr, len, prot, flags, fildes, off)`

munmap -- unmap pages of memory
`int munmap(void *addr, size_t len);`

operations take much time because:
* search tree takes logarithm to find free memory
* same pages are often reused so get mapped and unmapped often
* page should get kicked out of TLB cache

if `malloc`ing for the first time -- you get zeroes;
if `malloc`ing the same page for the second time -- there might be traces of previous information

all modern allocators use the same algorithm: red-black tree

for small objects allocators linked list (using stack) is [used](https://github.com/mtrebi/memory-allocators):

![linked list](linked-list.png)

###### N.B

LD_PRELOAD = {.so} -- [preload shared libraries](https://stackoverflow.com/questions/426230/what-is-the-ld-preload-trick)

###### end of N.B

[small string optimization](https://stackoverflow.com/questions/10315041/meaning-of-acronym-sso-in-the-context-of-stdstring/10319672#10319672):
when string size is small (e.g. <16) its data is stored in static on-stack object.
otherwise only pointer to the data in RAM stored

[fbstring trick](https://stackoverflow.com/questions/45900169/does-fbstrings-small-string-optimization-rely-on-undefined-behavior)

[malloc-intercept](https://github.com/sorokin/malloc-intercept)

#### copy-on-write optimization

When data is the same it points to the same address.
Once one copy of data is to be changed, it is copied to another place in memory

### everything you wanted to know about operator `new` but were afraid to ask

1. `new T(1, 2, 3) / delete p        // new-expression`

    calls `operator new`: `p = new T(1, 2, 3)` =>
```
    p = operator new(sizeof(T));
    if (p) {
        try { p -> T(1, 2, 3); }
        catch (...) {
            operator delete(p);
            throw;
        }
    }
```

1. malloc(100) / free(p)
1. constructor / destructor
1. void *p = operator new(100) / operator delete(p)

```
    void * operator new (size_t size) {
        void * res = malloc(size);
        if (!res)
            throw std::bad_alloc();
        return res;
    }
```

#### syntax of `new`

`new (1, 2, 3) T(4, 5, 6)`:
  * `new T(1, 2, 3)`
  * `new (std::nothrow) T(1, 2, 3)`
  * `void * operator new(size_t size, void * location) { return location; }  // placement new`

