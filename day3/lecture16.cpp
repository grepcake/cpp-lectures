#include <cstdlib>
#include <algorithm>

#define DECLARE_VECTOR(type) \
    struct vector_##type{};

DECLARE_VECTOR(int)

vector_int foo;

template<typename T>
struct foo {
    T *data;
    size_t size;
    size_t capacity;
};

template<typename T>
T const &max(T const &a, T const &b) {
    return a < b ? a : b;
}

int const &max(const int &a, const int &b) { // no compilation error
    return a < b ? a : b;
}

int main() {
    return 0;
}

template<typename T>
struct vector {      // primary template
};

template<>
struct vector<bool> { // explicit specialization
};

template<typename U>
struct vector<U *> { // partial specialization
};

