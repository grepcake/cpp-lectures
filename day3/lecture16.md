# Templates

### ways to make generic container:

* `void *`
    1. smaller object files
    1. easier to debug
    1. faster compilation time
    1. might not lead to multiple definitions
* code generation (e.g., via preprocessor)
    1. type-safe
    1. memory-effective (for `void *` every type object is stored in memory)

    ```
    #define DECLARE_VECTOR(type) \
        struct vector_##type{};

    DECLARE_VECTOR(int)
    ```

* templates
    1. type-safe
    1. memory-effective
    1. easier to debug
    1. mignt not lead to multiple definitions

### template classes:

    ```
    template<typename T>
    struct vector {
        T *data;
        size_t size;
        size_t capacity;
    };
    ```

### template functions:

    T const &max(T const &a, T const &b) {
        return a < b ? a : b;
    }

Template functions are inlined.
Thus they are usually defined in headers.
However, in Cfront by Bjarne Stroustrup template functions were defined in source files.

Don't do [template export](https://stackoverflow.com/questions/5416872/using-export-keyword-with-templates)

### Going deeper

dependent names -- if used compiler supposes that variable if no `typename` present:

`(T::foo)-b` -- binary minus, `(typename T::foo)-b` -- cast

`a < b > c` -- might be `a<b> c` if `a` is template or `(a < b) > c` otherwise.

`(T::foo < b) > c` and  `T::template foo <b> c;`

##### primary template

```
template<typename T>
struct vector {
};
```

##### explicit specialization

```
template<>
struct vector<bool> {
};
```

##### partial specialization

```
template<typename U>
struct vector<U *> {
};
```

#### specialization clashing

```
template <typename T>
struct mytype;

template <typename U>
struct mytype<U *>;

template <typename U>
struct mytype<U **>;
```

most specialized definition is chosen. `U**` specializes `U*`

```
template <typename T, typename P>
struct mytype;

template <typename U, typename Q>
struct mytype<U *, Q>;

template <typename U, typename R>
struct mytype<U, R*>;

mytype<int *, int *> foo;// error: no most specialized option
```

#### template constants (nontype parameters)

```
template <typename T, size_t N>
struct array {};
```

#### default template parameters

```
template <typename T, typename A = std::allocator<T>>
struct vector;
```

#### other

templates are means of polymorphism just like inheritance

Although, templates increase compilation time, arguments calls might be inlined unlike virtual calls with inheritance.
Thus, templates are usually more efficient.

Templates represent static polymorphism, inheritance -- dynamic polymorphism.

When should I use inheritance?
If I need to store a collection of elements of different types but with the same ancestor -- the only way to do it is via inheritance
