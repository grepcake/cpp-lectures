#include <cstddef>
#include <cstdio>
#include <vector>
#include <cstdint>

using size_t

/* generated assembly both stores result in a register and writes to memory at `result`
 * it is needed in case `result` points to the same memory as `arr` */

void sum(float *result, float const *arr, size_t n) {
    for (std::size_t i = 0; i < n; ++i) {
        *result += arr[i];
    }
}

void memcpy(char *dst, char const *src, size_t size) {
    for (std::size_t i = 0; i < size; ++i) {
        dst[i] = src[i];
    }
}

/* Classic example of UB */
void f(float *p) {
    float unused = *p;

    if (!p) { return; }  // `p` was dereferenced earlier, so compiler assumes it is not nullptr

    printf("%f", *p);
}

/* example #2 */
int d[16];

int SATD() {
    int satd = 0, dd, k;
    for (dd = d[k = 0]; k < 16; dd = d[++k]) { // size of `d` is 16, compiler assumes that there is no UB,
        satd += (dd < 0 ? -dd : dd);           // so k is always less than 16
    }
    return satd;
}

/* example #3 */
bool f(int a) { return (a + 1) < a; } // always false


/* TBAA */
template<typename T>
void f(std::vector<T> const &v) {
    T sum = T();
    for (std::size_t i = 0; i < v.size(); ++i) {
        v[i] = T();
    }
}

template<>
void f(std::vector<int> const &v) {}  //  expected assembly
template<>
void f(std::vector<char> const &v) {} //  assembly is bloated because `char *data` in vector might alias this vector

struct masked_char {
    char c;
};

void f(std::vector<masked_char> const &v) {} // not bloated because `masked_char` doesn't alias anything

std::uint32_t swap_words(std::uint32_t in) {
    uint16_t *p = (uint16_t *) &in;
    uint16_t temp = p[0];
    p[0] = p[1];
    p[1] = temp;
    return in;
}
