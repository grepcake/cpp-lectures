#include <type_traits>
#include <iostream>

template<typename C>
void f(C &c, typename C::iterator i);

template<typename E, size_t N>
void f(E (&c)[N], E *i);

template<bool v, typename T>
struct enable_if;

template<typename T>
struct enable_if<true, T> {
    typedef T type;
};

template<typename T>
typename enable_if<std::is_array_v<T>, void>::type
print(T const &v) {
    std::cout << v[0] << std::endl;
}

template<typename T>
typename enable_if<not std::is_array_v<T>, void>::type
print(T const &v) {
    std::cout << v << std::endl;
}

int main() {
    int a[10] = {1, 2, 3};
    print(a);
    print(5);
}
