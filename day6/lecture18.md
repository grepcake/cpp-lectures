# STL overlook

* parallel_reduce for associative operations

* generic programming by [Alexander Stepanov](https://en.wikipedia.org/wiki/Alexander_Stepanov)

    <!-- * ![him](Alexander-Stepanov.jpg) -->

    * Elements Of Programming

## STL structure

* containers
    * sequences
        * vector
        * list
        * deque
    * associative contatiners
        * \[unordered]\[multi]{set|map}

* iterators (common interface for movement in containers)
    * operations
        * `*it`: take underlying object
        * `it1 == it2`: determine if iterators point to the same place
        * `++it`: take next object
        * `container.begin()`: iterator to the first object
        * `container.end()`: iterator to the place after the last object
    * types
        * [InputIterator](http://en.cppreference.com/w/cpp/concept/InputIterator):
         read but once
        * [OutputIterator](http://en.cppreference.com/w/cpp/concept/OutputIterator):
         write but once
        * [ForwardIterator](http://en.cppreference.com/w/cpp/concept/ForwardIterator):
         might be forked
        * [BidirectionalIterator](http://en.cppreference.com/w/cpp/concept/BidirectionalIterator):
         has `--it` operations
        * [RandomAccessIterator](http://en.cppreference.com/w/cpp/concept/RandomAccessIterator):
         has `it +/- n` and comparison operations
     * pointers are iterators

* algorithms (designed to work with many containers)
    * find: linear lookup

* container adapters
    * stack
    * queue
    * priority_queue (heap queue)

* functors / functional objects
    * `std::less`

## iterators

* reverse iterators
    * `rbegin()`: points to the last element
    * `rend()`: points to the place before first element
    * `++reverse_it`: travels one position behind
    * `reverse_it.base()`: underlying direct iterator
        * `rbegin().base` = `end()`; `rend().base` = `begin()`

* `lower_bound`, `upper_bound` for binary search

## type traits

```
   template <typename T>
   struct numeric_limits;

   template <>
   struct numeric_limits<int> {
       int max() { return ...; }
   };
   ```

* [`<type_traits>`](http://en.cppreference.com/w/cpp/header/type_traits)

* tag dispatching
    ```
    #include <type_traits>
    #include <iostream>

    template<bool V>
    struct is_array_tag {
    };

    template<typename T>
    void print_impl(T const &v, is_array_tag<false>) {
        std::cout << v << std::endl;
    }

    template<typename T>
    void print_impl(T const &v, is_array_tag<true>) {
        std::cout << v[0] << std::endl;
    }

    template<typename T>
    void print(T const &v) {
        print_impl(v, is_array_tag<std::is_array_v<T>>());
    }

    int main() {
        int a[] = {1, 2, 3};
        print(a);
        print(5);
    }
    ```

* SFINAE: Substitution Failure Is Not An Error
    ```
        template<typename C>
       void f(C &c, typename C::iterator i);

       template<typename E, size_t N>
       void f(E (&c)[N], E *i);

       int main() {
           int a[10] = {1, 2, 3};
           f(a, a + 5);
       }
   ```

    * `enable_if`
        ```
           template<bool v, typename T>
           struct enable_if;

           template<typename T>
           struct enable_if<true, T> {
               typedef T type;
           };

           template<typename T>
           typename enable_if<std::is_array_v<T>, void>::type
           print(T const &v) {
               std::cout << v[0] << std::endl;
           }

           template<typename T>
           typename enable_if<not std::is_array_v<T>, void>::type
           print(T const &v) {
               std::cout << v << std::endl;
           }

           int main() {
               int a[10] = {1, 2, 3};
               print(a);
               print(5);
           }
        ```
