#include <typeinfo>
#include <iostream>

struct base {
    bool is_derived() const;

    virtual ~base();
};

struct derived;

void f(base &obj) {
    if (obj.is_derived()) {
        derived &d = (derived &) obj; // non valid cast: base and derived are not connected
//        d.g();
    }
}

struct derived : base {
    void g();
};


int main() {
    int const a = 5;
    const_cast<int &>(a) = 6;
    std::cout << a * a << std::endl; // might print 25 instead of 36
}