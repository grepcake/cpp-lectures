## Namespaces

* means to avoid name clashing

* ```
    namespace foo {
        void bar();
        void baz() { bar(); }
    }
    foo::bar();
  ```

* `using`
    * directive

      ```
        using namespace foo;
        bar();
        baz();
      ```

    * declaration

      ```
        using foo::bar;
        bar();
        foo::baz();
        // baz();   -- fails
      ```

    * ```
        namespase n {
            using std::vector;
            using foo::vector;

            vector v; // compilation error
        }
      ```

    * ```
        namespace n {
            using std;
            using foo;

            vector v; // runtime error
        }
      ```

    * ```
        namespace n {
            using std;
            using foo;
            using foo::vector;

            vector v; // no error, vector from foo;
        }
      ```

* type alias

  ```
    namespace n {
        using namespace std;
        using namespace foo;
        template <typename T>
        using myvector = std::vector<T>;

        myvector v; // no error, vector from std;
    }
  ```

* namespace alias

  ```
    namespace fs = std::filesystem;
  ```

* `static` as a keyword to mark something
local to the current translation unit.
It is deprecated in C++.
You should rather use anonymous namespaces

    * ```
        namespace { int q; }
        foo<&q>();
      ```

* lookup in classes

    * ```
        struct base { void f(); };
        struct derived : base {
            using base::f; // so that derived().f() would work
            void f(int);
        };
      ```

    * ```
        struct base {
        protected:
            void f();
        }

        struct derived: base {
        public:
            using base::f; // f is now public
        }
      ```

## ADL (Argument Dependent Lookup)

* also Koeing lookup

Operators for a class are usually put into some namespace.
However, it is undesirable to specify this namespace everytime
need for these operators arises. Thus, ADL was presented.

ADL allows to look function up in namespaces of its arguments
even if they are not included into lookup via `using`.

```
int main() {
    n::big_integer a, b;
    a + b; // no need for `using n::operator+`
}
```

```
void f();

namespace n {
    void f();
    void g() {
        ::f(); // calls global f
    }
}
```

## C++ style casts

`xxx_cast<type>(expr)`

* static_cast
    * might cast number types: `long` <-> `int`
    * might cast between hierarchies: `derived` <->`base`
    * might cast pointers: `type*` <-> `void*`

* reintrepret_cast
    * disouraged to use

* const_cast
    * might mislead compiler (see code)
    * fine to apply to variables that were not declared `const` initially

* dynamic_cast
    * cast from base to derived with hierarchy if possible
